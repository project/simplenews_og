
Description
-----------------
This module integrates Simplenews and Organic groups.
It adds a 'Group newsletters' tab to group home pages where privileged users may
create/manage/send group newsletters.

Simplenews provides several admin pages by default:
- Manage your subscriptions (to subscribe/unsubscribe the current user to/from any newsletter):
  -- Dedicated page (also allows anonymous users to subscribe): newsletter/subscriptions
  -- Tab on the user edit form: user/%user/edit/simplenews
- Manage newsletters nodes (list/edit/send and view status of any newsletter): admin/content/simplenews
- Manage newsletter categories (add/edit/delete any category): admin/config/services/simplenews/categories
- Newsletter node tab (to send/test a newsletter): node/%node/simplenews
- Manage newsletters subscribers:
  -- Subscribers list (activate/inactivate/edit/delete any subscriber): admin/people/simplenews
  -- Mass subscribe (subscribe a list of email addresses to a newsletter category): admin/people/simplenews/import
  -- Mass unsubscribe (unsubscribe a list of email addresses from a newsletter category): admin/people/simplenews/unsubscribe
  -- Export subscribers (email addresses): admin/people/simplenews/export
- Configuration:
  -- Default newsletter options, test addresses, newsletter info: admin/config/services/simplenews
  -- Subscription settings, opt-in/out confirmation email text: admin/config/services/simplenews/settings/subscription
  -- Send mail, cron and debug options: admin/config/services/simplenews/settings/mail

This module integrates OG 7.x-2.x and Simplenews 7.x-1.x.
It adds several OG admin pages so that a group user with the OG permission 'Administer group newsletters'
can create/edit/send newsletters, manage subscribers and configure newsletter settings of his own group(s).
- Manage group newsletters content (list/edit/send and view status of group newsletters): group/%/%/admin/simplenews
- Manage group newsletters subscribers:
  -- Subscribers list (unsubscribe any subscriber): group/%/%/admin/simplenews/subscribers
  -- Mass subscribe (subscribe a list of email addresses to the group newsletter): group/%/%/admin/simplenews/subscribers/import
  -- Mass unsubscribe (unsubscribe a list of email addresses from the group newsletter): group/%/%/admin/simplenews/subscribers/unsubscribe
  -- Export group newsletter subscribers (email addresses): group/%/%/admin/simplenews/subscribers/export
- Manage group newsletter configuration: group/%/%/admin/simplenews/settings
