<?php

/**
 * @file
 * Admin page callbacks for the simplenews_og module.
 */

/******************************************************************************/
/*                         Group newsletters content                          */
/******************************************************************************/

/**
 * Menu callback: Manage group newsletters content.
 */
function _simplenews_og_admin_issues($form, &$form_state, $group_type, $gid) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));
  $group = entity_load_single($group_type, $gid);

  // Store OG group type and entity.
  $form['group_type'] = array('#type' => 'value', '#value' => $group_type);
  $form['group_entity'] = array('#type' => 'value', '#value' => $group);

  // Retrieve associated Simplenews category tid.
  $category = _simplenews_og_load_category($group_type, $gid);
  if (!$category) {
    // Nothing found: display the form to create the category.
    $form['description'] = array(
      '#markup' => '<p><strong>' . t('Newsletters for this group are not yet enabled. Fill in this form to enable the feature. All active members of this group will be automatically subscribed.') . '</strong></p>',
      '#weight' => -20,
    );

    // Reuse the manage group newsletter settings form and pass default values.
    $edit = array(
      'name' => $group->title . ' newsletter',
      'description' => t('Newsletter category for the @title group.', array('@title' => $group->title)),
    );
    $form += drupal_get_form('_simplenews_og_admin_category_form', $group_type, $gid, $edit);
    $form['simplenews']['actions']['submit']['#value'] = t('Enable newsletters for this group');

    // Add a submit handler to register active og members.
    $form['#submit'][] = '_simplenews_og_create_category_submit';
  }
  else {
    // A category is associated with this group: display all newsletter nodes.
    module_load_include('inc', 'simplenews', 'includes/simplenews.admin');

    // Add content create links.
    $category_field = variable_get('simplenews_category_field', 'field_simplenews_term');
    $items = array();
    foreach (node_type_get_types() as $name => $type) {
      if (variable_get('simplenews_content_type_' . $name, FALSE) && og_is_group_content_type('node', $name) && og_user_access($group_type, $gid, "create $name content")) {
        $items[] = array(
          'data' => l($type->name, 'node/add/' . str_replace('_', '-', $type->type), array(
            'query' => array($category_field => $category->tid),
          )),
        );
      }
    }

    if (!empty($items)) {
      $form['create_links'] = array(
        '#type' => 'fieldset',
        '#title' => t('Create new newsletter content'),
        '#prefix' => '<div class="container-inline">',
        '#suffix' => '</div>',
      );
      $form['create_links']['content'] = array(
        '#theme' => 'item_list',
        '#items' => $items,
      );
    }

    // Build an 'Update options' form.
    $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update options'),
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    );
    $options = array();
    foreach (module_invoke_all('simplenews_issue_operations') as $operation => $array) {
      $options[$operation] = $array['label'];
    }
    $form['options']['operation'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => 'activate',
    );
    $form['options']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#submit' => array('simplenews_admin_issues_submit'),
      '#validate' => array('simplenews_admin_issues_validate'),
    );

    if (variable_get('simplenews_last_cron', '')) {
      $form['last_sent'] = array(
        '#markup' => '<p>' . format_plural(variable_get('simplenews_last_sent', 0), 'Last batch: 1 mail sent at !time.', 'Last batch: !count mails sent at !time.', array(
          '!time' => format_date(variable_get('simplenews_last_cron', ''), 'small'),
          '!count' => variable_get('simplenews_last_sent', 0),
        )) . "</p>\n",
      );
    }
    // Table header. Used as tablesort default.
    $header = array(
      'title' => array('data' => t('Title'), 'field' => 'n.title'),
      'category' => array(
        'data' => t('Newsletter category'),
        'field' => 'sc.name',
      ),
      'created' => array(
        'data' => t('Created'),
        'field' => 'n.created',
        'sort' => 'desc',
      ),
      'published' => array('data' => t('Published')),
      'sent' => array('data' => t('Sent')),
      'subscribers' => array('data' => t('Subscribers')),
      'operations' => array('data' => t('Operations')),
    );

    $query = db_select('node', 'n')->extend('PagerDefault')->extend('TableSort');
    $query->join('simplenews_newsletter', 'sn', 'n.nid = sn.nid');
    $query->join('simplenews_category', 'sc', 'sn.tid = sc.tid');
    $query->join('taxonomy_term_data', 't', 'sc.tid = t.tid');
    $query->condition('sc.tid', $category->tid);
    $query->fields('n', array('nid', 'title', 'created', 'status'))
      ->fields('sn', array('tid'))
      ->fields('t', array('name'))
      ->limit(30)
      ->orderByHeader($header);
    $query->addField('sn', 'status', 'sent_status');

    $options = array();

    module_load_include('inc', 'simplenews', 'includes/simplenews.mail');
    foreach ($query->execute() as $issue) {
      $categories = simplenews_category_list();
      $subscriber_count = simplenews_count_subscriptions($issue->tid);
      $pending_count = simplenews_count_spool(array('nid' => $issue->nid));
      $send_status = $issue->sent_status == SIMPLENEWS_STATUS_SEND_PENDING ? $subscriber_count - $pending_count : theme('simplenews_status', array('source' => 'sent', 'status' => $issue->sent_status));

      $options[$issue->nid] = array(
        'title' => l($issue->title, 'node/' . $issue->nid),
        'category' => $issue->tid && isset($categories[$issue->tid]) ? $categories[$issue->tid] : t('- Unassigned -'),
        'created' => format_date($issue->created, 'small'),
        'published' => theme('simplenews_status', array('source' => 'published', 'status' => $issue->status)),
        'sent' => $send_status,
        'subscribers' => $subscriber_count,
        'operations' => l(t('edit'), 'node/' . $issue->nid . '/edit', array('query' => drupal_get_destination())),
      );
    }

    $form['issues'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No newsletters available.'),
    );

    $form['pager'] = array('#theme' => 'pager');
  }

  return $form;
}

/**
 * Submit handler to create Simplenews category.
 */
function _simplenews_og_create_category_submit($form, &$form_state) {
  $group_type = $form['group_type']['#value'];
  $group = $form['group_entity']['#value'];
  list($gid,,) = entity_extract_ids($group_type, $group);

  // Retrieve the category created by
  // _simplenews_og_admin_category_form_submit().
  $category = _simplenews_og_load_category($group_type, $gid);
  drupal_set_message(t('A new newsletter category has been created for this group: %name', array('%name' => $group->title . ' newsletter')));

  // Subscribe all OG members to the category.
  $memberships = og_membership_load_multiple(FALSE, array(
    'group_type' => $group_type,
    'gid' => $gid,
    'entity_type' => 'user',
    'state' => OG_STATE_ACTIVE,
  ));

  if (!empty($memberships)) {
    $uids = array();
    foreach ($memberships as $membership) {
      $uids[] = $membership->etid;
    }

    _simplenews_og_users_mass_subscribe($category, $uids);
  }
}

/**
 * Make mass update of users.
 */
function _simplenews_og_users_mass_subscribe($category, $uids) {
  // We use batch processing to prevent timeout when updating a large number
  // of users.
  if (count($uids) > 10) {
    $batch = array(
      'operations' => array(
        array(
          '_simplenews_og_users_subscribe_batch_process', array($category, $uids),
        ),
      ),
      'finished' => '_simplenews_og_users_subscribe_batch_finished',
      'title' => t('Processing'),
      // We use a single multi-pass operation, so the default
      // 'Remaining x of y operations' message will be confusing here.
      'progress_message' => '',
      'error_message' => t('An error occured.'),
      // The operations do not live in the .module file, so we need to
      // tell the batch engine which file to load before calling them.
      'file' => drupal_get_path('module', 'simplenews_og') . '/includes/simplenews_og.admin.inc',
    );
    batch_set($batch);
  }
  else {
    foreach ($uids as $uid) {
      _simplenews_og_mass_subscribe_helper($category, $uid);
    }
    drupal_set_message(t('All active members in this group have been automatically subscribed.'));
  }
}

/**
 * Simplenews OG Mass Update - helper function.
 */
function _simplenews_og_mass_subscribe_helper($category, $uid) {
  $account = user_load($uid);
  simplenews_subscribe_user($account->mail, $category->tid, FALSE, 'website');
}

/**
 * Simplenews OG Mass Update Batch operation.
 */
function _simplenews_og_users_subscribe_batch_process($category, $uids, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($uids);
    $context['sandbox']['users'] = $uids;
  }

  // Process by groups of 5.
  $count = min(5, count($context['sandbox']['users']));
  for ($i = 1; $i <= $count; $i++) {
    $uid = array_shift($context['sandbox']['users']);
    _simplenews_og_mass_subscribe_helper($category, $uid);

    // Store result for post-processing in the finished callback.
    $context['results'][] = $uid;

    // Update our progress information.
    $context['sandbox']['progress']++;
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Simplenews OG Mass Update Batch 'finished' callback.
 */
function _simplenews_og_users_subscribe_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('All active members in this group have been automatically subscribed.'));
  }
  else {
    drupal_set_message(t('An error occurred and processing did not complete.'), 'error');
    $message = format_plural(count($results), '1 item successfully processed:', '@count items successfully processed:');
    drupal_set_message($message);
  }
}

/******************************************************************************/
/*                       Group newsletters subscribers                        */
/******************************************************************************/

/**
 * Menu callback: Manage group newsletters subscribers form.
 */
function _simplenews_og_admin_subscription($form, &$form_state, $group_type, $gid) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));

  module_load_include('inc', 'simplenews', 'includes/simplenews.admin');
  $form['filter'] = simplenews_subscription_filter_form();
  $form['filter']['filters']['list']['#access'] = FALSE;
  $form['#submit'][] = 'simplenews_subscription_filter_form_submit';
  $form['filter']['#theme'] = 'simplenews_filter_form';

  $category = _simplenews_og_load_category($group_type, $gid);
  $form['category']['#value'] = $category;
  $form['admin'] = _simplenews_og_subscription_list_form($category);

  return $form;
}

/**
 * Form: Manage group newsletters subscribers helper.
 */
function _simplenews_og_subscription_list_form($category) {
  // Build an 'Update options' form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $options = array();
  foreach (module_invoke_all('simplenews_og_subscription_operations', $category) as $operation => $array) {
    $options[$operation] = $array['label'];
  }

  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'activate',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('_simplenews_og_subscription_list_form_submit'),
    '#validate' => array('_simplenews_og_subscription_list_form_validate'),
  );

  // Table header. Used as tablesort default.
  $header = array(
    'mail' => array('data' => t('Email'), 'field' => 'sn.mail', 'sort' => 'asc'),
    'username' => array('data' => t('Username'), 'field' => 'u.name'),
    'status' => array('data' => t('Status'), 'field' => 'sn.activated'),
    'language' => array('data' => t('Language'), 'field' => 'sn.language'),
  );

  $query = db_select('simplenews_subscriber', 'sn')->extend('PagerDefault')->extend('TableSort');
  $query->leftJoin('users', 'u', 'sn.uid = u.uid');
  $query->innerJoin('simplenews_subscription', 'su', 'sn.snid = su.snid');
  $query->condition('su.status', SIMPLENEWS_SUBSCRIPTION_STATUS_SUBSCRIBED);
  $query->condition('su.tid', $category->tid);
  $query->addField('u', 'name', 'name');
  $result = $query
    ->fields('sn', array('snid', 'activated', 'mail', 'uid', 'language'))
    ->limit(30)
    ->orderByHeader($header)
    ->execute();

  $options = array();
  foreach ($result as $subscriber) {
    $options[$subscriber->snid] = array(
      'mail' => check_plain($subscriber->mail),
      'username' => isset($subscriber->uid) ? l($subscriber->name, 'user/' . $subscriber->uid) : check_plain($subscriber->name),
      'status' => theme('simplenews_status', array('source' => 'activated', 'status' => $subscriber->activated)),
      'language' => check_plain($subscriber->language),
    );
  }

  $form['subscribers'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No subscribers available.'),
  );

  $form['pager'] = array(
    // Calling theme('pager') directly so that it the first call after the
    // pager query executed above.
    '#markup' => theme('pager'),
  );

  return $form;
}

/**
 * Form validation handler for _simplenews_og_subscription_list_form().
 */
function _simplenews_og_subscription_list_form_validate($form, &$form_state) {
  if (isset($form_state['values']['operation'])) {
    $snids = array_keys(array_filter($form_state['values']['subscribers']));
    if (empty($snids)) {
      form_set_error('', t('No items selected.'));
    }
  }
}

/**
 * Submit handler for _simplenews_og_subscription_list_form().
 */
function _simplenews_og_subscription_list_form_submit($form, &$form_state) {
  $category = $form['category']['#value'];

  // Call all hook_simplenews_og_subscription_operations() hooks.
  $operations = module_invoke_all('simplenews_og_subscription_operations', $category);
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked subscribers.
  $snids = array_filter($form_state['values']['subscribers']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($snids), $operation['callback arguments']);
    }
    else {
      $args = array($snids);
    }
    call_user_func_array($function, $args);

    drupal_set_message(t('The update has been performed.'));
  }
  else {
    // We need to rebuild the form to go to a second step. For example, to
    // show the confirmation form for the deletion of nodes.
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Implements hook_simplenews_og_subscription_operations().
 */
function simplenews_og_simplenews_og_subscription_operations($category) {
  $operations = array(
    'unsubscribe' => array(
      'label' => t('Unsubscribe'),
      'callback' => '_simplenews_og_subscription_unsubscribe',
      'callback arguments' => array($category),
    ),
  );
  return $operations;
}

/**
 * Callback function to un-subscribe a subscriber from a category.
 */
function _simplenews_og_subscription_unsubscribe($snids, $category) {
  foreach ($snids as $snid) {
    $subscriber = simplenews_subscriber_load($snid);
    simplenews_unsubscribe_user($subscriber->mail, $category->tid, FALSE, 'website');
  }
}

/******************************************************************************/
/*               Group newsletters subscribers (mass subscribe)               */
/******************************************************************************/

/**
 * Menu callback: Group newsletter subscribers (mass subscribe) form.
 */
function _simplenews_og_admin_subscription_list_add($form, &$form_state, $group_type, $gid) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));
  $group = entity_load_single($group_type, $gid);

  // Store OG group type and entity.
  $form['group_type'] = array('#type' => 'value', '#value' => $group_type);
  $form['group_entity'] = array('#type' => 'value', '#value' => $group);

  // Reuse the original Simplenews mass subscribe form.
  module_load_include('inc', 'simplenews', 'includes/simplenews.admin');
  $form += simplenews_subscription_list_add($form, $form_state);
  $form['newsletters']['#access'] = FALSE;

  return $form;
}

/**
 * Submit handler for _simplenews_og_admin_subscription_list_add().
 */
function _simplenews_og_admin_subscription_list_add_submit($form, &$form_state) {
  $group_type = $form['group_type']['#value'];
  $group = $form['group_entity']['#value'];
  list($gid,,) = entity_extract_ids($group_type, $group);

  $category = _simplenews_og_load_category($group_type, $gid);
  $form_state['values']['newsletters'] = array($category->tid => TRUE);
  simplenews_subscription_list_add_submit($form, $form_state);

  $form_state['redirect'] = "group/$group_type/$gid/admin/simplenews/subscribers";
}

/******************************************************************************/
/*             Group newsletters subscribers (mass unsubscribe)               */
/******************************************************************************/

/**
 * Menu callback: Group newsletter subscribers (mass unsubscribe) form.
 */
function _simplenews_og_admin_subscription_list_remove($form, &$form_state, $group_type, $gid) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));
  $group = entity_load_single($group_type, $gid);

  // Store OG group type and entity.
  $form['group_type'] = array('#type' => 'value', '#value' => $group_type);
  $form['group_entity'] = array('#type' => 'value', '#value' => $group);

  // Reuse the original Simplenews mass unsubscribe form.
  module_load_include('inc', 'simplenews', 'includes/simplenews.admin');
  $form += simplenews_subscription_list_remove($form, $form_state);
  $form['newsletters']['#access'] = FALSE;

  return $form;
}

/**
 * Submit handler for _simplenews_og_admin_subscription_list_remove().
 */
function _simplenews_og_admin_subscription_list_remove_submit($form, &$form_state) {
  $group_type = $form['group_type']['#value'];
  $group = $form['group_entity']['#value'];
  list($gid,,) = entity_extract_ids($group_type, $group);

  $category = _simplenews_og_load_category($group_type, $gid);
  $form_state['values']['newsletters'] = array($category->tid => TRUE);
  simplenews_subscription_list_remove_submit($form, $form_state);

  $form_state['redirect'] = "group/$group_type/$gid/admin/simplenews/subscribers";
}

/******************************************************************************/
/*                  Group newsletters subscribers (export)                    */
/******************************************************************************/

/**
 * Menu callback: Group newsletters subscribers (export) form.
 */
function _simplenews_og_admin_subscription_list_export($form, &$form_state, $group_type, $gid) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));
  $group = entity_load_single($group_type, $gid);

  // Store OG group type and entity.
  $form['group_type'] = array('#type' => 'value', '#value' => $group_type);
  $form['group_entity'] = array('#type' => 'value', '#value' => $group);

  // Reuse the original Simplenews export form.
  module_load_include('inc', 'simplenews', 'includes/simplenews.admin');
  $form += simplenews_subscription_list_export($form, $form_state);
  $form['newsletters']['#access'] = FALSE;
  $form['states']['#access'] = FALSE;

  return $form;
}

/**
 * Submit handler for _simplenews_og_admin_subscription_list_export().
 */
function _simplenews_og_admin_subscription_list_export_submit($form, &$form_state) {
  $group_type = $form['group_type']['#value'];
  $group = $form['group_entity']['#value'];
  list($gid,,) = entity_extract_ids($group_type, $group);
  $category = _simplenews_og_load_category($group_type, $gid);
  $form_state['values']['newsletters'] = array($category->tid => TRUE);

  // Get data for query string and redirect back to the current page.
  $form_values = $form_state['values'];
  $options['query']['states'] = array_filter($form_values['states']);
  $options['query']['subscribed'] = array_filter($form_values['subscribed']);
  $options['query']['newsletters'] = array_keys(array_filter($form_values['newsletters']));
  $form_state['redirect'] = array("group/$group_type/$gid/admin/simplenews/subscribers/export", $options);
}

/******************************************************************************/
/*                         Group newsletters settings                         */
/******************************************************************************/

/**
 * Menu callback: Manage group newsletter settings.
 */
function _simplenews_og_admin_category_form($form, &$form_state, $group_type, $gid, $edit = NULL) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));
  $group = entity_load_single($group_type, $gid);

  // Store OG group type and entity.
  $form['group_type'] = array('#type' => 'value', '#value' => $group_type);
  $form['group_entity'] = array('#type' => 'value', '#value' => $group);

  // Load the category.
  $edit = $edit ? $edit : _simplenews_og_load_category($group_type, $gid);

  // Reuse the original Simplenews category edit form.
  module_load_include('inc', 'simplenews', 'includes/simplenews.admin');
  $form['simplenews'] = simplenews_admin_category_form($form, $form_state, $edit);

  // Add the original validate handler (validates the email).
  $form['#validate'][] = 'simplenews_admin_category_form_validate';

  // Remove the delete option.
  unset($form['simplenews']['actions']['delete']);

  return $form;
}

/**
 * Form submit callback for a newsletter category form.
 */
function _simplenews_og_admin_category_form_submit($form, &$form_state) {
  $category = (object) $form_state['values'];

  $term = taxonomy_term_load($form_state['values']['tid']);
  if (!$term) {
    $term = new stdClass();
  }

  $term->tid = $form_state['values']['tid'];
  $term->vocabulary_machine_name = 'newsletter';
  $term->vid = taxonomy_vocabulary_machine_name_load('newsletter')->vid;
  $term->name = $form_state['values']['name'];
  $term->description = $form_state['values']['description'];
  $term->weight = $form_state['values']['weight'];

  // Create or update membership between taxonomy term and OG group.
  $group_type = $form['group_type']['#value'];
  $group = $form['group_entity']['#value'];
  list($gid,,) = entity_extract_ids($group_type, $group);
  $field_name = SIMPLENEWS_OG_AUDIENCE_FIELD;
  $wrapper = entity_metadata_wrapper('taxonomy_term', $term);
  $wrapper->{$field_name}->set($gid);

  taxonomy_term_save($term);
  $category->tid = $term->tid;
  simplenews_category_save($category);

  $form_state['values']['tid'] = $category->tid;
  $form_state['tid'] = $category->tid;
}
